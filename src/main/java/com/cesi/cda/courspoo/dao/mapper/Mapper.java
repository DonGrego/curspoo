package com.cesi.cda.courspoo.dao.mapper;

import com.cesi.cda.courspoo.dao.meet.dto.MeetDto;
import com.cesi.cda.courspoo.dao.meet.model.Meet;
import com.cesi.cda.courspoo.dao.personne.dto.PersonneDto;
import com.cesi.cda.courspoo.dao.personne.model.Personne;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class Mapper {

    public static List<Meet> mapDao(List<MeetDto> meetDtos){
        return meetDtos.stream()
                .map(meetDto -> {
                    Meet meet = new Meet();
                    meet.setNumeroPerdant(meetDto.getNumeroPerdant());
                    meet.setNumeroGagnant(meetDto.getNumeroGagnant());
                    meet.setAnneeMatch(meetDto.getAnneeMatch());
                    meet.setLieuRencontre(meetDto.getLieuRencontre());
                    return meet;
                }).toList();
    }

    public static List<Personne> mapPersonneDao(List<PersonneDto> personneDtos){
        return personneDtos.stream()
                .map(personneDto -> {
                    Personne personne = new Personne();
                    personne.setId(personneDto.getId());
                    personne.setNom(personneDto.getNom());
                    personne.setPrenom(personneDto.getPrenom());
                    personne.setAnneeNaissance(personneDto.getAnneeNaissance());
                    return personne;
                }).toList();

    }

    public static Personne mapPersonneDaoId (PersonneDto personneDto){

                    Personne personne = new Personne();
                    personne.setId(personneDto.getId());
                    personne.setNom(personneDto.getNom());
                    personne.setPrenom(personneDto.getPrenom());
                    personne.setAnneeNaissance(personneDto.getAnneeNaissance());
                    return personne;

    }


}
