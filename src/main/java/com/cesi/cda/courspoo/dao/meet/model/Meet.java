package com.cesi.cda.courspoo.dao.meet.model;

public class Meet {
    public int id;

    public int NumeroGagnant;
    public int NumeroPerdant;
    public String lieuRencontre;
    public Long anneeMatch;

    public int getId() {
        return id;
    }



    public int getNumeroGagnant() {
        return NumeroGagnant;
    }

    public int getNumeroPerdant() {
        return NumeroPerdant;
    }

    public String getLieuRencontre() {
        return lieuRencontre;
    }

    public Long getAnneeMatch() {
        return anneeMatch;
    }
    public void setNumeroGagnant(int numeroGagnant) {
        NumeroGagnant = numeroGagnant;
    }
    public void setNumeroPerdant(int numeroPerdant) {
        NumeroPerdant = numeroPerdant;
    }

    public void setLieuRencontre(String lieuRencontre) {
        this.lieuRencontre = lieuRencontre;
    }

    public void setAnneeMatch(Long anneeMatch) {
        this.anneeMatch = anneeMatch;
    }
}
