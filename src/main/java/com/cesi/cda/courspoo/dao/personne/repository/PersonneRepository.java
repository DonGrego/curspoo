package com.cesi.cda.courspoo.dao.personne.repository;

import com.cesi.cda.courspoo.dao.mapper.Mapper;
import com.cesi.cda.courspoo.dao.personne.dto.PersonneDto;
import com.cesi.cda.courspoo.dao.personne.model.Personne;
import com.cesi.cda.courspoo.dao.personne.model.PersonneDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestClient;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.Arrays;
import java.util.List;

@Repository
public class PersonneRepository {

    @Autowired
    private Mapper mapper;

    private WebClient webclient;

    private RestTemplate restTemplate;

    public PersonneRepository(WebClient.Builder webClientBuilder) {
        this.webclient = webClientBuilder.baseUrl("https://8443-cesi2022-spring3-nk8oef9q6ta.ws-eu110.gitpod.io/api/v1").build();
    }

    public List<PersonneDto> getPersonneDto(){
        String apiUrl = "/persons";
        RestClient restClient = RestClient.create();
        String URL = "https://8443-cesi2022-spring3-nk8oef9q6ta.ws-eu110.gitpod.io/api/v1";
        PersonneDto[] result =
                restClient.get()
                .uri(URL + apiUrl)
                .retrieve()
                .body(PersonneDto[].class);

        return Arrays.stream(result).toList();

    }

    public PersonneDto getPersonneDtoBydId(int id){
        String apiUrl = "/persons/"+id;
        RestClient restClient = RestClient.create();
        String URL = "https://8443-cesi2022-spring3-nk8oef9q6ta.ws-eu110.gitpod.io/api/v1";
        PersonneDto result =
                restClient.get()
                        .uri(URL + apiUrl)
                        .retrieve()
                        .body(PersonneDto.class);
        return result;

    }



    public List<Personne> getPersonne(){
        try{
            List<PersonneDto> personneDtos = getPersonneDto();
            return mapper.mapPersonneDao(personneDtos);

        }catch (Error e){
            throw new Error(e);
        }

    }

    public Personne getPersonneById(int id){
        try{
            PersonneDto personneDto = getPersonneDtoBydId(id);
            return mapper.mapPersonneDaoId(personneDto);

        }catch (Error e){
            throw new Error(e);
        }

    }

    public void postPersonne (Personne personne) {
        try {
            String apiUrl = "/persons";
            RestTemplate restTemplate = new RestTemplate();
            String URL = "https://8443-cesi2022-spring3-nk8oef9q6ta.ws-eu110.gitpod.io/api/v1";
            restTemplate.postForObject(URL + apiUrl, personne, Void.class);


        } catch (Error e) {
            throw new Error(e);
        }


    }
}
