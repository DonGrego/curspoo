package com.cesi.cda.courspoo.dao.personne.dto;

public class PersonneDto {
    public int id;
    public String prenom;
    public String nom;
    public int anneeNaissance;
    public String nationalite;

    public int getId() {
        return id;
    }

    public String getPrenom() {
        return prenom;
    }

    public String getNom() {
        return nom;
    }

    public int getAnneeNaissance() {
        return anneeNaissance;
    }
}
