package com.cesi.cda.courspoo.dao.meet.dto;

public class MeetDto {

    int Id;
    Integer NumeroGagnant;
    int NumeroPerdant;
    String lieuRencontre;
    Long anneeMatch;

    public int getId() {
        return Id;
    }

    public int getNumeroGagnant() {
        return NumeroGagnant;
    }

    public int getNumeroPerdant() {
        return NumeroPerdant;
    }

    public String getLieuRencontre() {
        return lieuRencontre;
    }

    public Long getAnneeMatch() {
        return anneeMatch;
    }

    public void setId(int id) {
        Id = id;
    }

    public void setNumeroGagnant(Integer numeroGagnant) {
        NumeroGagnant = numeroGagnant;
    }

    public void setNumeroPerdant(int numeroPerdant) {
        NumeroPerdant = numeroPerdant;
    }

    public void setLieuRencontre(String lieuRencontre) {
        this.lieuRencontre = lieuRencontre;
    }

    public void setAnneeMatch(Long anneeMatch) {
        this.anneeMatch = anneeMatch;
    }
}
