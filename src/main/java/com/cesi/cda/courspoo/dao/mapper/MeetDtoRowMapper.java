package com.cesi.cda.courspoo.dao.mapper;

import com.cesi.cda.courspoo.dao.meet.dto.MeetDto;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class MeetDtoRowMapper implements RowMapper<MeetDto> {
        @Override
        public MeetDto mapRow(ResultSet resultSet, int rowNum) throws SQLException {
            MeetDto meetDto = new MeetDto();
            meetDto.setId(resultSet.getInt("Id"));
            meetDto.setNumeroGagnant(resultSet.getInt("NuGAGNANT"));
            meetDto.setNumeroPerdant(resultSet.getInt("NUPERDANT"));
            meetDto.setLieuRencontre(resultSet.getString("LIEUTOURNOI"));
            meetDto.setAnneeMatch(resultSet.getLong("ANNEE"));
            return meetDto;
        }
}
