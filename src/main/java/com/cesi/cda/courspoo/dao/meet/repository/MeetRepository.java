package com.cesi.cda.courspoo.dao.meet.repository;

import com.cesi.cda.courspoo.dao.mapper.Mapper;
import com.cesi.cda.courspoo.dao.mapper.MeetDtoRowMapper;
import com.cesi.cda.courspoo.dao.meet.dto.MeetDto;
import com.cesi.cda.courspoo.dao.meet.model.Meet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public class MeetRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private Mapper mapper;

    public List<MeetDto> getMeetDto() {
        try {
            String sql = "SELECT * FROM RENCONTRE";
            return jdbcTemplate.query(sql, new MeetDtoRowMapper());

        } catch (Error e) {
            throw new Error(e);
        }
    }
    public List<Meet> getMeet(){
        try{
            List<MeetDto> meetDtos = getMeetDto();
            return mapper.mapDao(meetDtos);

        }catch (Error e){
            throw new Error(e);
        }
    }

    public void save(Meet meet) {
        try {
            String sql = "INSERT INTO RENCONTRE (`id`,`NUGAGNANT`, `NUPERDANT`, `LIEUTOURNOI`, `ANNEE`) VALUES (:id, :nugagnant, :nuperdant, :lieutournoi, :annee)";
            var parameters = new MapSqlParameterSource();
            parameters.addValue("id", meet.getId());
            parameters.addValue("nugagnant", meet.getNumeroGagnant());
            parameters.addValue("nuperdant", meet.getNumeroPerdant());
            parameters.addValue("lieutournoi", meet.getLieuRencontre());
            parameters.addValue("annee", meet.getAnneeMatch());

            jdbcTemplate.update(sql, parameters);

        } catch (Error e) {
            throw new Error(e);
        }
    }
}

