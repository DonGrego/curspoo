package com.cesi.cda.courspoo.dao.personne.model;

public class PersonneDao {
    public String prenom;
    public String nom;
    public int anneeNaissance;

    public String nationalite;

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setAnneeNaissance(int anneeNaissance) {
        this.anneeNaissance = anneeNaissance;
    }


    public void setNationalite(String nationality){this.nationalite = nationalite;}

    public String getPrenom() {
        return prenom;
    }

    public String getNom() {
        return nom;
    }

    public int getAnneeNaissance() {
        return anneeNaissance;
    }

    public String getNationalite() {return nationalite;}
}
