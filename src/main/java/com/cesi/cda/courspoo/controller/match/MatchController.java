package com.cesi.cda.courspoo.controller.match;

import com.cesi.cda.courspoo.business.match.impl.MatchBusiness;
import com.cesi.cda.courspoo.controller.model.Match;

import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/match")
public class MatchController {

    @Autowired
    MatchBusiness matchBusiness;

    @GetMapping
    public List<Match> getMatch(){
        return matchBusiness.getMatch();
    }

    @GetMapping("/id")
    public Match getMatchById(@RequestParam Integer id){
        return matchBusiness.getMatchById();
    }

    @PostMapping
    public Match postMatch(@RequestBody Match match){
        matchBusiness.postMatch(match);
        return match;
    }

    @DeleteMapping("/id")
    public String deleteMatchById(@RequestParam Integer id){
        return "c'est supprimé";
    }
}
