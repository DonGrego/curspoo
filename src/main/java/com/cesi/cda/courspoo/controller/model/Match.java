package com.cesi.cda.courspoo.controller.model;




public class Match {
    int Id;
    String nomPrenomGagnant;
    String nomPrenomPerdant;
    Long dateLieuRencontre;
    int sommeAges;

    public Match(int Id, String nom, String nom1, Long anneeMatch, int age) {
    }

    public Match() {

    }

    public String getNomPrenomGagnant() {
        return nomPrenomGagnant;
    }

    public String getNomPrenomPerdant() {
        return nomPrenomPerdant;
    }

    public Long getDateLieuRencontre() {
        return dateLieuRencontre;
    }

    public int getSommeAges() {
        return sommeAges;
    }

    public int getId() {return  Id;}

    public void setNomPrenomGagnant(String nomPrenomGagnant) {
        this.nomPrenomGagnant = nomPrenomGagnant;
    }

    public void setNomPrenomPerdant(String nomPrenomPerdant) {
        this.nomPrenomPerdant = nomPrenomPerdant;
    }

    public void setDateLieuRencontre(long dateLieuRencontre) {
        this.dateLieuRencontre = dateLieuRencontre;
    }

    public void setSommeAges(int sommeAges) {
        this.sommeAges = sommeAges;
    }
}
