package com.cesi.cda.courspoo.business.match.impl;

import com.cesi.cda.courspoo.controller.model.Match;
import com.cesi.cda.courspoo.dao.meet.model.Meet;
import com.cesi.cda.courspoo.dao.meet.repository.MeetRepository;
import com.cesi.cda.courspoo.dao.personne.model.Personne;
import com.cesi.cda.courspoo.dao.personne.model.PersonneDao;
import com.cesi.cda.courspoo.dao.personne.repository.PersonneRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MatchBusiness {

    @Autowired
    MeetRepository meetRepository;

    @Autowired
    PersonneRepository personneRepository;



    public List<Match> getMatch(){

        List<Meet> meets = getMeet();
        List<Personne> personnes = getPersonne();
        List<Match> allMatch = createMatch(meets, personnes);

        return allMatch;
    };

    public List<Match> createMatch( List<Meet> meets, List<Personne> personnes){
        List<Match> matches = new ArrayList<>();

        for (Meet meet : meets) {
            Personne winner = findPersonneById(meet.getNumeroGagnant(), personnes);
            Personne loser = findPersonneById(meet.getNumeroPerdant(), personnes);

            Match match = new Match();
            match.setNomPrenomGagnant(winner.getNom() + " " + winner.getPrenom());
            match.setNomPrenomPerdant(loser.getNom() + " " + loser.getPrenom());
            match.setDateLieuRencontre(meet.getAnneeMatch());
            match.setSommeAges(getAge(winner.getAnneeNaissance()) + getAge(loser.getAnneeNaissance()));
            matches.add(match);
           }


        return matches;
    }

    public Match postMatch(Match match){

        Match newMatch = new Match();
        newMatch.setNomPrenomGagnant(match.getNomPrenomGagnant());
        newMatch.setNomPrenomPerdant(match.getNomPrenomPerdant());
        newMatch.setSommeAges(match.getSommeAges());
        newMatch.setDateLieuRencontre(match.getDateLieuRencontre());
        newMatch.setSommeAges(match.getSommeAges());


        Personne personne1 = new Personne();

        personne1.setNom(newMatch.getNomPrenomGagnant());
        personne1.setPrenom(newMatch.getNomPrenomGagnant());
        personne1.setAnneeNaissance(1990);
        personne1.setNationalite("FR");

        personneRepository.postPersonne( personne1);

        Personne personne2 = new Personne();
        personne2.setNom(newMatch.getNomPrenomPerdant());
        personne2.setPrenom(newMatch.getNomPrenomPerdant());
        personne2.setAnneeNaissance(1990);
        personne2.setNationalite("FR");

        personneRepository.postPersonne(personne2);

        Meet newMeet = new Meet();
        newMeet.setAnneeMatch(match.getDateLieuRencontre());
        newMeet.setNumeroGagnant(personne1.getId());
        newMeet.setNumeroPerdant(personne2.getId());
        newMeet.setLieuRencontre("Rolland");
        newMeet.setAnneeMatch(match.getDateLieuRencontre());

        meetRepository.save(newMeet);


        return newMatch;

    }

    private Personne findPersonneById(int personneId, List<Personne> personnes) {
        return personnes.stream()
                .filter(personne -> personne.getId() == personneId)
                .findFirst()
                .orElse(null);
    }



    public Match getMatchById(){
        Match match = new Match();
        return match;
    };
    public List<Meet> getMeet(){
        return meetRepository.getMeet();
    }

    public List<Personne> getPersonne(){
        return personneRepository.getPersonne();
    }

    public Personne getPersonneByName(int id){
        return personneRepository.getPersonneById(id);
    }

    private static int getAge(int naissance) {
        return 2024 - naissance;
    }


}
