package com.cesi.cda.courspoo;

import com.cesi.cda.courspoo.business.match.impl.MatchBusiness;
import com.cesi.cda.courspoo.controller.match.MatchController;
import com.cesi.cda.courspoo.controller.model.Match;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class MatchControllerTest {

    @Mock
    private MatchBusiness matchBusiness;

    @InjectMocks
    private MatchController matchController;

    @Test
    public void testGetMatch(){
        List<Match> matchList = new ArrayList<>();
        matchList.add(new Match(1, "Grégoire","Gregou", 1990L, 380));
        when(matchBusiness.getMatch()).thenReturn(matchList);

        List<Match> result = matchController.getMatch();

        assertEquals(result, matchList);
    }
}
